[![pipeline status](https://gitlab.com/ayys/nginx-and-njs-dockerized/badges/master/pipeline.svg)](https://gitlab.com/ayys/nginx-and-njs-dockerized/commits/master)


# Nginx and Njs Dockerized

Contains Nginx and Njs inside Docker


* Nginx Version - 1.17.2


## Included modules

* [Redis2 For Nginx](https://github.com/openresty/redis2-nginx-module)
* [Njs](https://github.com/simplresty/ngx_devel_kit)
* [ngx_set_misc](https://github.com/openresty/set-misc-nginx-module)
