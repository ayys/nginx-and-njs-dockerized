#!/usr/bin/bash

bash -c "cd njs ; ./configure"

./configure\
	--add-module=redis2-nginx-module\
	--add-module=ngx_devel_kit\
	--add-module=set-misc-nginx-module\
	--add-module=njs/nginx\
	--with-zlib=zlib-master\
	--with-http_ssl_module\
	--with-http_v2_module\
	--with-http_image_filter_module\
	--with-http_gzip_static_module\
	--with-http_auth_request_module\
	--prefix=/usr\
	--modules-path=/usr/share/nginx/modules\
	--conf-path=/etc/nginx/nginx.conf\
	--error-log-path=/var/log/nginx/error.log\
	--sbin-path=/usr/local/bin\
	--pid-path=/var/log/nginx/nginx.pid\
	--http-log-path=/var/log/nginx/access.log

make -j2
make install
