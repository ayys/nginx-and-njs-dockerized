function authentication(r) {
    function getData(res) {
	if (res.responseBody.includes(":1"))
	    r.return (200, "Passed Authentication"); // nginx auth module requires 200 on success
	else r.return (401, "Failed Authentication"); // nginx auth module requires 401/403 on failure
    }
    r.subrequest("/redis-auth-test", {method: "GET"} , getData);
}
