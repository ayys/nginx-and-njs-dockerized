FROM alpine:latest AS builder
# RUN apk add --update build-base git
## Download and decpmpress nginx
# RUN apk add build-base gcc make libc-dev musl-dev sudo pcre-dev gd-dev
RUN apk --update add build-base git gd-dev unzip pcre-dev openssl-dev bash
WORKDIR /nginx
COPY . /nginx
RUN tar xvf nginx-1.17.2.tar.gz && mv nginx-1.17.2/* /nginx
# Download required stuff
RUN unzip zlib.zip && sh INSTALL.sh
# Decompress all the downloaded stuff


## -------------------------------------------------------------------------- ##

FROM alpine
WORKDIR /nginx
RUN mkdir -p /etc/nginx /var/log/nginx
COPY --from=builder /nginx/objs/nginx /nginx
COPY --from=builder /etc/nginx/ /etc/nginx/
RUN apk add --update --no-cache gd-dev pcre-dev openssl-dev

COPY nginx.conf /etc/nginx/
COPY auth.js /etc/nginx/
CMD ["/nginx/nginx"]
